
function symbols = BPSK_MOD(data_scrambled, carrier)

    % Make binary bit into BPSK symbols - 1 bit/symbol
    symbols = zeros(1,carrier.num_bits);
    for i=1:length(symbols)
       if (data_scrambled(i)==0)
          symbols(i) = -1 + 0j;
       else
          symbols(i) = 1 + 0j;
       end
    end
end