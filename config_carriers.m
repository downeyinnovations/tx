
function carriers = config_carriers(config)
    
    carriers = [];
    fns = fieldnames(config);
    fns_size = size(fns);
    adc = config.adc;
    fprintf('Generating snapshot of %i samples\n', adc.num_samples);
    
    % Loop through each field name of configuration structure to see if
    % there is a carrier present
    num_car = 0;
    for i=1:fns_size(1)
        field = string(fns(i));
        if(contains(field, 'carrier'))
            c = config.(fns{i});
            
            num_car = num_car + 1;
            fprintf('Carrier %i CF: %i Symrate: %i C/N: %i Mod: %s\n', num_car, c.cf, c.symrate, c.c_to_n, c.mod);
            
            % Add samples to account for delay through pulse shaping filter
            % Since filter is FIR, delay = coefficients/2
            c.snap_size = adc.num_samples + adc.psf_size/2;
            
            c.fsamp = adc.fsamp;
            c.spb = ceil(adc.fsamp/c.symrate); % Samples per symbol
            c.num_bits = ceil(c.snap_size/c.spb);  % Assuming 1 bit per symbol
                
            % Adjust for greater than 1 bit/sym
            if(strcmp(c.mod, 'QPSK'))
                c.num_bits = c.num_bits*2;
            elseif(strcmp(c.mod, '8PSK') || strcmp(c.mod, '8QAM'))
                c.num_bits = c.num_bits*3;
            elseif(strcmp(c.mod, '16PSK') || strcmp(c.mod, '16APSK') || strcmp(c.mod, '16QAM'))
                c.num_bits = c.num_bits*4;
            elseif(strcmp(c.mod, '32APSK'))
                c.num_bits = c.num_bits*5;
            end
            
            carriers = [carriers, c];
            
        end
    end