%% Complex FFT

function [freq] = ComplexFFT(data, fftsize, numavg)

    N = length(data);
    
    % Throw error if the fftsize is greater than the data size
    if(fftsize > N)
        ME = MException('MyComponent:invalidInput', ...
            'fftsize of %i is larger than data of size %i', fftsize, N);
        throw(ME);
    end
    
    % Adjust number of averages if data isn't big enough
    if (numavg*fftsize > N)
        numavg = floor(N/fftsize);
        fprintf('Number of averages has been changed to %i because not enough samples\n', numavg);
    end
    
    % Can't have zero averaging
    if (numavg == 0)
        numavg = 1;
        fprintf('Number of averages was 0 and has been changed to 1\n');
    end
    
    freq = zeros(1,fftsize);
    for i=1:numavg
        d = data((i-1)*fftsize+1:i*fftsize);
        fft_calc = abs(fftshift(fft(d)));
        freq = freq + fft_calc;
    end

    freq = freq/(numavg*fftsize*fftsize); % normalize FFT output
    freq = db(freq);

    %len = length(freq);
   
    %xaxis = -pi:2*pi/(len-1):pi;
    %xaxis = -50:100/(len-1):50;
    %figure;
    %plot(xaxis,freq);
    %xlim([-pi,pi]);
    %ylim([-30,70]);
    %title(figureTitle);
end


