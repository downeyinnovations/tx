
% https://www.mathworks.com/help/signal/ug/resampling.html

function signal = upsample_carrier(signal, carrier, factor)
    
    if(mod(carrier.fsamp, carrier.symrate) ~= 0)
        r = floor(carrier.fsamp/carrier.symrate);
        p = floor(abs(rem(carrier.fsamp/carrier.symrate, 1))*10^factor);
        q = 10^factor;
        signal = repeat_samples(signal, r);   % Upsample amount
        signal = resample(signal, p, q);
    else
        signal = repeat_samples(signal, carrier.fsamp/carrier.symrate);
    end
end