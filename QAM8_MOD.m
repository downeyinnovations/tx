
function symbols = QAM8_MOD(data_scrambled, carrier)

    symbols = zeros(1,carrier.num_bits/3);

    n = 1; % integer to loop through data bits
    
    % Make binary bits into 8QAM symbols - 3 bits/symbol
    for i=1:length(symbols)

        bit_seq = data_scrambled(1,n:n+2);

        if(all(bit_seq==[0 0 0]))
            symbols(i) = -3 + 0j;
        elseif(all(bit_seq==[0 0 1]))
            symbols(i) = -1 - 1j;
        elseif(all(bit_seq==[0 1 0]))
            symbols(i) = -1 + j;
        elseif(all(bit_seq==[0 1 1]))
            symbols(i) = 0 - j;
        elseif(all(bit_seq==[1 1 0]))
            symbols(i) = 0 + j;
        elseif(all(bit_seq==[1 1 1]))
            symbols(i) = 1 - j;
	    elseif(all(bit_seq==[1 0 0]))
            symbols(i) = 1 + j;
        elseif(all(bit_seq==[1 0 1]))
            symbols(i)= 3 + 0j;
        end
    
        n=n+3;

    end
end