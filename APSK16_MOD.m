
function symbols = APSK16_MOD(data_scrambled, carrier)
    
    symbols = zeros(1,carrier.num_bits/4);

    n = 1; % integer to loop through data bits
    gamma = 3;
    r2 = 1;
    r1 = r2/gamma;

    % Make binary bits into 16APSK symbols - 4 bits/symbol
    for i=1:length(symbols)
        
        bit_seq = data_scrambled(1,n:n+3);

        % Inner ring R1
        if(all(bit_seq==[1 1 0 0]))
            symbols(i) = r1*cos(pi/4) + j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 1 1 0]))
            symbols(i) = -r1*cos(pi/4) + j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 1 1 1]))
            symbols(i) = -r1*cos(pi/4) - j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 1 0 1]))
            symbols(i) = r1*cos(pi/4) - j*r1*sin(pi/4);

        % Outer ring R2  
        elseif(all(bit_seq==[0 1 0 0]))
            symbols(i) = r2*cos(pi/12) + j*r2*sin(pi/12);
        elseif(all(bit_seq==[0 0 0 0]))
            symbols(i) = r2*cos(3*pi/12) + j*r2*sin(3*pi/12);
	    elseif(all(bit_seq==[1 0 0 0]))
            symbols(i) = r2*cos(5*pi/12) + j*r2*sin(5*pi/12);
        elseif(all(bit_seq==[1 0 1 0]))
            symbols(i) = r2*cos(7*pi/12) + j*r2*sin(7*pi/12);
        elseif(all(bit_seq==[0 0 1 0]))
            symbols(i) = r2*cos(9*pi/12) + j*r2*sin(9*pi/12);
        elseif(all(bit_seq==[0 1 1 0]))
            symbols(i) = r2*cos(11*pi/12) + j*r2*sin(11*pi/12);
        elseif(all(bit_seq==[0 1 1 1]))
            symbols(i) = r2*cos(13*pi/12) + j*r2*sin(13*pi/12);
        elseif(all(bit_seq==[0 0 1 1]))
            symbols(i) = r2*cos(15*pi/12) + j*r2*sin(15*pi/12);
        elseif(all(bit_seq==[1 0 1 1]))
            symbols(i) = r2*cos(17*pi/12) + j*r2*sin(17*pi/12);
        elseif(all(bit_seq==[1 0 0 1]))
            symbols(i) = r2*cos(19*pi/12) + j*r2*sin(19*pi/12);
        elseif(all(bit_seq==[0 0 0 1]))
            symbols(i) = r2*cos(21*pi/12) + j*r2*sin(21*pi/12);
        elseif(all(bit_seq==[0 1 0 1]))
            symbols(i) = r2*cos(23*pi/12) + j*r2*sin(23*pi/12);   
        end

        n=n+4;
       
    end
end