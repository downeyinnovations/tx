%Glowlink Communications Technology, Inc
%Copyright 2000


function[sig]= trans_sim(trans_bw,start_freq,stop_freq,fsamp,num_samples,...
   	num_carrier,mod,c_to_n,baud_in,cf,alpha_in,offset_f,clip_point)
 			
%%%%Add 128 sample to the num_samples b/c of the filter
num_samples= num_samples + 128;

%%%%Inititalize
sig = zeros(1,num_samples);
x = zeros(1,length(num_samples));

for i=1:num_carrier 
   spb= fsamp/baud_in(i); 
   spb =ceil(spb);
   num_bits= num_samples/spb;
   
   %%Make sure num_bits is round to near interger
   num_bits = round(num_bits);
   baud = baud_in(i);
   alpha = alpha_in(i);
   
   if(mod(i)==1| mod(i)==7)
      num_bits = num_bits;
	elseif (mod(i) ==2 | mod(i)==6 )
      num_bits = num_bits*2;
   elseif (mod(i) ==3)
      num_bits = num_bits*3;
   elseif (mod(i)==4 | mod(i)==5)
      num_bits = num_bits*4;
   end
   
   %%%Type of modulation
   type_mod= mod(i);
   
   center_freq = (stop_freq - start_freq)/2;
   cf_carrier= cf(i)+offset_f;
   carrier_diff = center_freq- cf_carrier;
   carrier_shift = (num_samples-128)*cf_carrier/(fsamp);

   max_cn= max(c_to_n);
   C_N = c_to_n(i);
   
   x = signal_generate(num_bits,spb,...
     carrier_shift,alpha,type_mod,num_samples,baud);
  
  if (i==1)
     Cpow = sum(x.*conj(x));
     cnLin = 10^(C_N/10);
     noisePower = ((Cpow/cnLin)*fsamp/(baud*(1+alpha)))/num_samples;
     noiseSTD = sqrt(noisePower);
  else
     carPow = sum(x.*conj(x));
     cnLin = 10^(C_N/10);
     noiseInBand = (((baud*(1+alpha))/fsamp)*num_samples)*noisePower;
     factor = sqrt((carPow/noiseInBand)/cnLin);
     x = x/factor;
  end
     
  if(length(x) > length(sig))
  	 x = x(1:length(sig));
  end

  sig = sig(1:length(x)) +x;
  if (i==2)
     xjunk = (x + conj(x));
     save interferer xjunk;
  end
  

end

sig = (sig + conj(sig));

%maxVal = max(max(sig));
%minVal = min(min(sig));

%maxVal=maxVal*.4;
%minVal = minVal*.4;
%for k=1:length(sig)
   %if (sig(k) > maxVal)
      %sig(k) = maxVal;
   %end
   %if (sig(k) < minVal)
     % sig(k) = minVal;
   %end
%end

%%%Add noise
rand('seed',sum(100*clock));
noise = randn(size(sig))*noiseSTD;
sig = sig + noise;


%if(clip_point ==0)
  % sig = sig;
%else
   %%%Clip the signal
%	sig = clip(sig,clip_point*10^-3);
%end
%%%Take the histogram for the signal
%hist1 = hist(sig,200);
%mf = medianFilter(hist1,11);
 
%%Average power of the signal
%power = sum(sig.*sig)/length(sig);
 
