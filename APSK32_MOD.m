
function symbols = APSK32_MOD(data_scrambled, carrier)
    
    symbols = zeros(1,carrier.num_bits/5);

    n = 1; % integer to loop through data bits
    gamma1 = 3; % R2/R1
    gamma2 = 5; % R3/R1
    r3 = 1;
    r1 = r3/gamma2;
    r2 = gamma1*r1;

    % Make binary bits into 32APSK symbols - 5 bits/symbol
    for i=1:length(symbols)
        
        bit_seq = data_scrambled(1,n:n+4);

        % Inner ring R1
        if(all(bit_seq==[1 0 0 0 1]))
            symbols(i) = r1*cos(pi/4) + j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 0 1 0 1]))
            symbols(i) = -r1*cos(pi/4) + j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 0 1 1 1]))
            symbols(i) = -r1*cos(pi/4) - j*r1*sin(pi/4);
        elseif(all(bit_seq==[1 0 0 1 1]))
            symbols(i) = r1*cos(pi/4) - j*r1*sin(pi/4);

        % Middle ring R2  
        elseif(all(bit_seq==[1 0 0 0 0]))
            symbols(i) = r2*cos(pi/12) + j*r2*sin(pi/12);
        elseif(all(bit_seq==[0 0 0 0 0]))
            symbols(i) = r2*cos(3*pi/12) + j*r2*sin(3*pi/12);
	    elseif(all(bit_seq==[0 0 0 0 1]))
            symbols(i) = r2*cos(5*pi/12) + j*r2*sin(5*pi/12);
        elseif(all(bit_seq==[0 0 1 0 1]))
            symbols(i) = r2*cos(7*pi/12) + j*r2*sin(7*pi/12);
        elseif(all(bit_seq==[0 0 1 0 0]))
            symbols(i) = r2*cos(9*pi/12) + j*r2*sin(9*pi/12);
        elseif(all(bit_seq==[1 0 1 0 0]))
            symbols(i) = r2*cos(11*pi/12) + j*r2*sin(11*pi/12);
        elseif(all(bit_seq==[1 0 1 1 0]))
            symbols(i) = r2*cos(13*pi/12) + j*r2*sin(13*pi/12);
        elseif(all(bit_seq==[0 0 1 1 0]))
            symbols(i) = r2*cos(15*pi/12) + j*r2*sin(15*pi/12);
        elseif(all(bit_seq==[0 0 1 1 1]))
            symbols(i) = r2*cos(17*pi/12) + j*r2*sin(17*pi/12);
        elseif(all(bit_seq==[0 0 0 1 1]))
            symbols(i) = r2*cos(19*pi/12) + j*r2*sin(19*pi/12);
        elseif(all(bit_seq==[0 0 0 1 0]))
            symbols(i) = r2*cos(21*pi/12) + j*r2*sin(21*pi/12);
        elseif(all(bit_seq==[1 0 0 1 0]))
            symbols(i) = r2*cos(23*pi/12) + j*r2*sin(23*pi/12);   

        % Outer ring R3
        elseif(all(bit_seq==[1 1 0 0 0]))
            symbols(i) = r3*cos(0) + j*r3*sin(0);
        elseif(all(bit_seq==[0 1 0 0 0]))
            symbols(i) = r3*cos(1*pi/8) + j*r3*sin(1*pi/8);
	    elseif(all(bit_seq==[1 1 0 0 1]))
            symbols(i) = r3*cos(2*pi/8) + j*r3*sin(2*pi/8);
        elseif(all(bit_seq==[0 1 0 0 1]))
            symbols(i) = r3*cos(3*pi/8) + j*r3*sin(3*pi/8);
        elseif(all(bit_seq==[0 1 1 0 1]))
            symbols(i) = r3*cos(4*pi/8) + j*r3*sin(4*pi/8);
        elseif(all(bit_seq==[1 1 1 0 1]))
            symbols(i) = r3*cos(5*pi/8) + j*r3*sin(5*pi/8);
        elseif(all(bit_seq==[0 1 1 0 0]))
            symbols(i) = r3*cos(6*pi/8) + j*r3*sin(6*pi/8);
        elseif(all(bit_seq==[1 1 1 0 0]))
            symbols(i) = r3*cos(7*pi/8) + j*r3*sin(7*pi/8);
        elseif(all(bit_seq==[1 1 1 1 0]))
            symbols(i) = r3*cos(8*pi/8) + j*r3*sin(8*pi/8);
        elseif(all(bit_seq==[0 1 1 1 0]))
            symbols(i) = r3*cos(9*pi/8) + j*r3*sin(9*pi/8);
        elseif(all(bit_seq==[1 1 1 1 1]))
            symbols(i) = r3*cos(10*pi/8) + j*r3*sin(10*pi/8);
        elseif(all(bit_seq==[0 1 1 1 1]))
            symbols(i) = r3*cos(11*pi/8) + j*r3*sin(11*pi/8);
        elseif(all(bit_seq==[0 1 0 1 1]))
            symbols(i) = r3*cos(12*pi/8) + j*r3*sin(12*pi/8);
        elseif(all(bit_seq==[1 1 0 1 1]))
            symbols(i) = r3*cos(13*pi/8) + j*r3*sin(13*pi/8);
        elseif(all(bit_seq==[0 1 0 1 0]))
            symbols(i) = r3*cos(14*pi/8) + j*r3*sin(14*pi/8);
        elseif(all(bit_seq==[1 1 0 1 0]))
            symbols(i) = r3*cos(15*pi/8) + j*r3*sin(15*pi/8);

        end

        n=n+4;
       
    end
end