
function signal = shift_signal(signal, freq_shift, samp_rate)

    phase = -2*pi*j*freq_shift/samp_rate;
    ivec = linspace(1,length(signal),length(signal));
    phase_change = exp(phase*ivec);
    signal = signal.*phase_change;