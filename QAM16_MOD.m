
function symbols = QAM16_MOD(data_scrambled, carrier)

    symbols = zeros(1,carrier.num_bits/4);

    n = 1; % integer to loop through data bits
    
    % Make binary bits into 16QAM symbols - 4 bits/symbol
    for i=1:length(symbols)

        bit_seq = data_scrambled(1,n:n+3);

        if(all(bit_seq==[0 0 0 0]))
            symbols(i) = -3 - 3j;
        elseif(all(bit_seq==[0 0 0 1]))
            symbols(i) = -1 - 3j;
        elseif(all(bit_seq==[0 1 0 0]))
            symbols(i) = -3 - j;
        elseif(all(bit_seq==[0 1 0 1]))
            symbols(i) = -1 - j;
        elseif(all(bit_seq==[0 1 1 1]))
            symbols(i) = 1 - j;
        elseif(all(bit_seq==[0 0 1 1]))
            symbols(i) = 1 - 3j;
	    elseif(all(bit_seq==[0 1 1 0]))
            symbols(i) = 3 - j;
        elseif(all(bit_seq==[0 0 1 0]))
            symbols(i)= 3 -3j;
        elseif(all(bit_seq==[1 1 1 1]))
            symbols(i) = 1 + j;
        elseif(all(bit_seq==[1 1 1 0]))
            symbols(i) = 3 + j;
	    elseif(all(bit_seq==[1 0 1 0]))
            symbols(i) = 3 + 3j;
        elseif(all(bit_seq==[1 0 1 1]))
            symbols(i) = 1 + 3j;
        elseif(all(bit_seq==[1 1 0 1]))
            symbols(i)= -1 + j;
        elseif(all(bit_seq==[1 0 0 1]))
            symbols(i) = -1 + 3j;
	    elseif(all(bit_seq==[1 1 0 0]))
            symbols(i) = -3 + j;
        elseif(all(bit_seq==[1 0 0 0]))
            symbols(i) = -3 + 3j;   
        end
    
        n=n+4;

    end
end


