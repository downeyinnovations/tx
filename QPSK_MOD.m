
function symbols = QPSK_MOD(data_scrambled, carrier)

    symbols = zeros(1,carrier.num_bits/2);

    n = 1; % integer to loop through data bits

    % Make binary bits into QPSK symbols - 2 bits/symbol
    for i=1:length(symbols)
        
        bit_seq = data_scrambled(1,n:n+1);

        if (all(bit_seq==[0 0]))
            symbols(i) = cos(pi/4) + j*sin(pi/4);
        elseif (all(bit_seq==[1 0]))
            symbols(i) = -cos(pi/4) + j*sin(pi/4);
        elseif (all(bit_seq==[0 1]))
            symbols(i) = cos(pi/4) - j*sin(pi/4);
        else 
            symbols(i) = -cos(pi/4) - j*sin(pi/4);   
        end

        n=n+2;

    end
end
