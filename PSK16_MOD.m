
function symbols = PSK16_MOD(data_scrambled, carrier)
    
    symbols = zeros(1,carrier.num_bits/4);

    n = 1; % integer to loop through data bits

    % Make binary bits into 16PSK symbols - 4 bits/symbol
    for i=1:length(symbols)
    
        bit_seq = data_scrambled(1,n:n+3);

        if(all(bit_seq==[0 0 0 0]))
            symbols(i) = cos(pi/16) + j*sin(pi/16);
        elseif(all(bit_seq==[0 0 0 1]))
            symbols(i) = cos(3*pi/16) + j*sin(3*pi/16);
	    elseif(all(bit_seq==[1 0 0 1]))
            symbols(i) = cos(5*pi/16) + j*sin(5*pi/16);
        elseif(all(bit_seq==[1 0 1 1]))
            symbols(i) = cos(7*pi/16) + j*sin(7*pi/16);
        elseif(all(bit_seq==[0 0 1 1]))
            symbols(i) = -cos(7*pi/16) + j*sin(7*pi/16);
        elseif(all(bit_seq==[0 0 1 0]))
            symbols(i) = -cos(5*pi/16) + j*sin(5*pi/16);
	    elseif(all(bit_seq==[1 0 1 0]))
            symbols(i) = -cos(3*pi/16) + j*sin(3*pi/16);
        elseif(all(bit_seq==[1 1 1 0]))
            symbols(i) = -cos(pi/16) + j*sin(pi/16);
        elseif(all(bit_seq==[0 1 1 0]))
            symbols(i) = -cos(pi/16) - j*sin(pi/16);
        elseif(all(bit_seq==[0 1 1 1]))
            symbols(i) = -cos(3*pi/16) - j*sin(3*pi/16);
	    elseif(all(bit_seq==[1 1 1 1]))
            symbols(i) = -cos(5*pi/16) - j*sin(5*pi/16);
        elseif(all(bit_seq==[1 1 0 1]))
            symbols(i) = -cos(7*pi/16) - j*sin(7*pi/16);
        elseif(all(bit_seq==[0 1 0 1]))
            symbols(i) = cos(7*pi/16) - j*sin(7*pi/16);
        elseif(all(bit_seq==[0 1 0 0]))
            symbols(i) = cos(5*pi/16) - j*sin(5*pi/16);
	    elseif(all(bit_seq==[1 1 0 0]))
            symbols(i) = cos(3*pi/16) - j*sin(3*pi/16);
        elseif(all(bit_seq==[1 0 0 0]))
            symbols(i) = cos(pi/16) - j*sin(pi/16);   
       end

       n=n+4;
       
    end
end
