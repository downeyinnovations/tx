
function  signal = signal_generate(carrier)

    % Generate random bitstream
    data = (rand(1,carrier.num_bits) <.5);

    % Scramble bitstream
    % TODO: Determine if this scrambling is even necessary
    N=round(log2(carrier.num_bits));
    shiftReg= ones(1,N);
    for i= 1:carrier.num_bits
       if(N <7)
          c = xor(shiftReg(1),shiftReg(3));
       else
          c = xor(shiftReg(4),shiftReg(7)); 
       end

       for j = N+1:-1:2
          shiftReg(j) = shiftReg(j-1);
       end
       shiftReg(1) = c;
       data_scrambled(i)= xor(data(i),c);
    end  

    % Modulate data
    if strcmp(carrier.mod,'BPSK') 
       signal = BPSK_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'QPSK')
       signal = QPSK_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'8PSK')
       signal = PSK8_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'8QAM')
       signal = QAM8_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'16PSK')
       signal = PSK16_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'16QAM')
       signal = QAM16_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'16APSK')
       signal = APSK16_MOD(data_scrambled, carrier);
    elseif strcmp(carrier.mod,'32APSK')
       signal = APSK32_MOD(data_scrambled, carrier);
    end

    %modulation_plotter(signal);

    signal = upsample_carrier(signal, carrier);

    % Pulse Shape w/ Root raised cosine 
    coef = rcosdesign(0.25, 24, carrier.spb/2);
    signal = filter(coef,1,signal);
    
    % FIR Pulse Shaping Filter
    %FC = 2*(1/carrier.spb);
    %limitFilter = fir1(256,FC);
    %signal = filter(limitFilter,1,signal); 

    % Upconvert
    signal = shift_signal(signal, -carrier.cf, carrier.fsamp);
    
end