% Downey Innovations LLC
% Copyright 2022

close all;
clear all;

import +yaml.*

% Load configuration file
config_file = 'test.yaml';
config = yaml.ReadYaml(config_file);

carriers = config_carriers(config);
snapshot = 0; % Samples containing all carriers & noise
snap_size = config.adc.num_samples + config.adc.psf_size/2;
Nt_std = 0.01;
Nt_avg = Nt_std*Nt_std;
Nt = Nt_avg*snap_size;
noise_floor = randn(1,snap_size)*Nt_std;
%nPow = sum(noise_floor.*conj(noise_floor));

for i=1:length(carriers)

    c = carriers(i);
       
    signal = signal_generate(c);
    
    if(length(signal) > c.snap_size)
        signal = signal(length(signal) - (c.snap_size-1):end);
    end
    
    cPow = sum(signal.*conj(signal)); % Complex signal power calc
    cnLin = 10^(c.c_to_n/10);    % C/N ratio in linear units
    
    % Want noise power only in carrier's spectrum
    % Nc = Nt*(bandwdith/fsamp) bandwidth = 2*symbol rate
    % Nt = Nc*fsamp/bandwidth
    Nc = Nt*2*c.symrate/c.fsamp; % Noise power only in carrier's spectrum
        
    % Attenuate/Amplify carrier to meet desired C/N ratio
    % attenuation*(cPow/Nc) = cnLin
    atten = cnLin*Nc/cPow;
    signal = signal*sqrt(atten);       
    snapshot = snapshot + real(signal);
    
end

snapshot = snapshot + noise_floor;

snapshot = normalize(snapshot, 'range', [-1 1]);

% Normalization creates a dc bias so remove bias
avg_snap = mean(snapshot);
snapshot = snapshot - avg_snap;

% Drop imaginary component and save samples
config.snapshot = snapshot;
yaml.WriteYaml('./test16M.txt', config);
