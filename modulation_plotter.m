
function [] = modulation_plotter(symbols)

    angles = linspace(0, 2*pi, 500);
    radius = 1;
    CenterX = 0;
    CenterY = 0;
    x = radius * cos(angles) + CenterX;
    y = radius * sin(angles) + CenterY;
    %plot(x, y, 'b-', 'LineWidth', 1);
    plot(x, y, 'b-');
    hold on;
    xline(0);
    yline(0);

    xlim([-4 4]);
    ylim([-4 4]);

    for i=1:length(symbols)
        x = real(symbols(i));
        y = imag(symbols(i));
        plot(x, y, '.', 'MarkerSize', 30, 'MarkerEdgeColor', [0 0 1]);
        hold on;
    end

    xlabel('I');
    ylabel('Q');

end

